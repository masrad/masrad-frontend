import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'
import Series from '@/models/series.js'
import About from '@/models/about.js'
import SeriesCollection from '@/collections/SeriesCollection.js'
import PublisherCollection from '@/collections/PublisherCollection.js'
import User from '@/models/user.js'
import Testimony from '@/models/testimony.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    about: null,
    series: null,               //this is equivalent to a 'collection' in the database
    seriesCollection: null,     //this is the set of collections (the whole archive, in collections)
    authError: Object,
    accessToken: null,
    currentUser: new User(),
    contentLanguages: null,
    selectedContentLanguage: null,
    currentTestimony: new Testimony(),
    useAnonVersion: true,
    publishers: null,
    save: false,
    lastSaved: null,
  },
  getters: {
    getAccessToken: state => { //eslint-disable-line
      return localStorage.getItem('accessToken')
    },
    getSelectedContentLanguage: state => {
      return state.selectedContentLanguage
    }
  },
  mutations: {
    setAbout (state, about) {
      state.about = about
    },
    setSeriesCollection (state, seriesCollection) {
      state.seriesCollection = seriesCollection
    },
    updateAccessToken (state, newToken) {
      localStorage.setItem('accessToken', newToken)
      state.accessToken = newToken
    },
    removeAccessToken (state) {
      localStorage.removeItem('accessToken')
      state.accessToken = null
    },
    setAuthError (state, error) {
      state.authError = error
    },
    clearAuthError (state) {
      state.authError = Object
    },
    setCurrentUser (state, user) {
      state.currentUser = user
    },
    setContentLanguages (state, languages) {
      state.contentLanguages = languages
    },
    setSelectedContentLanguage (state, language) {
      state.selectedContentLanguage = language
    },
    setCurrentTestimony (state, testimony) {
      state.currentTestimony = testimony
    },
    setSeries (state, series) {
      state.series = series
    },
    setUseAnonVersion (state, useAnonVersion) {
      state.useAnonVersion = useAnonVersion
    },
    setPublicFile (state, publicState) {
      state.currentTestimony = publicState
    },
    setPublishers (state, publishers) {
      state.publishers = publishers
    },
    setSave (state, save) {
      state.save = save
    },
    setLastSaved (state, lastSaved) {
      state.lastSaved = lastSaved
    }
  },
  actions: {
    async getAbout (context) {
      let about = new About()
      await about.fetch().then(() => {
        context.commit('setAbout', about)
      }).catch((error) =>  console.log(error)) //eslint-disable-line
    },
    async getSeriesCollection (context) {
      let seriesCollection = new SeriesCollection()
      await seriesCollection.fetch().then(() => {
        context.commit('setSeriesCollection', seriesCollection)
      }).catch((error) =>  console.log(error)) //eslint-disable-line
    },
    async getPublishers (context) {
      let publishers = new PublisherCollection()
      await publishers.fetch().then(() => {
        context.commit('setPublishers', publishers)
      })
    },
    async updateCurrentTestimony (context) {
      await context.state.currentTestimony.fetch()
                                          .then(function () {
                                            if (context.state.currentTestimony.anonymized_file) {
                                              context.commit('setUseAnonVersion', true)
                                            } else {
                                              context.commit('setUseAnonVersion', false)
                                            }
                                          })
                                          .catch( function(error) {
                                              console.log(error) //eslint-disable-line
                                              // TODO: if not found, then redirect to a testimony not found page
                                              // https://router.vuejs.org/guide/essentials/navigation.html#router-push-location-oncomplete-onabort
                                            })
    },
    async obtainAccessToken(context, authData){
      axios.post('/token-auth/', authData)
        .then((response) => {
          this.commit('updateAccessToken', response.data.token)
        })
        .catch((error) => {
          this.commit('setAuthError', error.response.data)
        })
    },
    async getContentLanguages () {
      await axios.get('/content-languages').then((response) => {
        this.commit('setContentLanguages', response.data)
      })
    },
    async updatePublicFileState(context, publicState) {
      context.state.currentTestimony.public = publicState
      await context.state.currentTestimony.save()
    },
    async saveTestimony(context) {
      await context.state.currentTestimony.save()
                                          .then(function () {
                                            if (context.state.currentTestimony.anonymized_file) {
                                              context.commit('setUseAnonVersion', true)
                                            } else {
                                              context.commit('setUseAnonVersion', false)
                                            }
                                          })
    },
    setSeries(context, series) {
      context.commit('setSeries', series)
    },
    async getSeries(context) {
      let series = new Series()
      await series.fetch().then(() => {
        context.commit('setSeries', series)
      }).catch((error) =>  console.log(error)) //eslint-disable-line
    },
    setCurrentTestimony(context, testimony) {
      context.commit('setCurrentTestimony', testimony)
    }
  }
})
