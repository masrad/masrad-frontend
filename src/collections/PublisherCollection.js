import { Collection } from 'vue-mc'
import Publisher from '@/models/publisher.js'

/**
 * Collection of publishers
 */

export default class PublisherCollection extends Collection {
  model () {
    return Publisher
  }

  routes () {
    return {
      fetch: '/publishers'
    }
  }
}
