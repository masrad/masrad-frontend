import { Collection } from 'vue-mc'
import Segment from '@/models/segment.js'

/**
 * Collection of segments
 */

export default class SegmentCollection extends Collection {
  defaults() {
    return {
      testimonyID: null,
    }
  }

  model () {
    return Segment
  }

  routes () {
    return {
      fetch: '/testimonies/{testimonyID}/segments'
    }
  }
}
