import { Collection } from 'vue-mc'
import Subject from '@/models/subject.js'

/**
 * Collection of users
 */

export default class SubjectCollection extends Collection {
  model () {
    return Subject
  }

  routes () {
    return {
      fetch: '/subjects'
    }
  }
}
