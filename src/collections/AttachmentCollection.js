import { Collection } from 'vue-mc'
import Attachment from '@/models/attachment.js'

/**
 * Collection of attachments
 */

export default class AttachmentCollection extends Collection {
  defaults() {
    return {
      testimonyID: null,
    }
  }

  model () {
    return Attachment
  }

  routes () {
    return {
      fetch: '/testimonies/{testimonyID}/attachments'
    }
  }
}
