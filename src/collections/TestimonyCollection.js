import { Collection } from 'vue-mc'
import Testimony from '@/models/testimony.js'

/**
 * Collection of testimonies
 */

export default class TestimonyCollection extends Collection {
  model () {
    return Testimony
  }

  routes () {
    return {
      fetch: '/testimonies'
    }
  }
}
