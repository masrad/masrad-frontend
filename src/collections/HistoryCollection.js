import { Collection } from 'vue-mc'
import History from '@/models/history.js'

/**
 * Collection of history records
 */

export default class HistoryCollection extends Collection {
  defaults() {
    return {
      testimonyID: null,
    }
  }

  model () {
    return History
  }

  routes () {
    return {
      fetch: '/testimonies/{testimonyID}/history/'
    }
  }
}
