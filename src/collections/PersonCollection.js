import { Collection } from 'vue-mc'
import Person from '@/models/person.js'

/**
 * Collection of testimonies
 */

export default class PersonCollection extends Collection {
  model () {
    return Person
  }

  routes () {
    return {
      fetch: '/persons'
    }
  }
}
