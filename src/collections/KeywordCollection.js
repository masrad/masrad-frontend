import { Collection } from 'vue-mc'
import Keyword from '@/models/keyword.js'

/**
 * Collection of keywords
 */

export default class KeywordCollection extends Collection {
  model () {
    return Keyword
  }

  routes () {
    return {
      fetch: '/keywords'
    }
  }
}
