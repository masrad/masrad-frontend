import { Collection } from 'vue-mc'
import Series from '@/models/series.js'

/**
 * Collection of testimonies
 */

export default class SeriesCollection extends Collection {
  model () {
    return Series
  }

  routes () {
    return {
      fetch: '/collections'
    }
  }
}
