import { Collection } from 'vue-mc'
import User from '@/models/user.js'

/**
 * Collection of users
 */

export default class EditorsCollection extends Collection {
  model () {
    return User
  }

  routes () {
    return {
      fetch: '/editors'
    }
  }
}
