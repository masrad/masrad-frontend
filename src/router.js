import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import Home from './views/Home.vue'
import TestimonyDetail from '@/components/TestimonyDetail'
import Office from '@/views/Office'
import OfficeHome from '@/views/OfficeHome'
import Login from '@/views/Login'
import About from '@/views/About'
import NewTestimony from '@/views/NewTestimony'
import EditTestimony from '@/views/EditTestimony'
import store from './store'
import Filter from '@/views/Filter'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/filter',
      name: 'filter',
      component: Filter
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/testimony/:id',
      name: 'testimony-detail',
      component: TestimonyDetail
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/office',
      component: Office,
      meta: { requiresAuth: true },
      children: [
      {
        path: '/',
        name: 'office',
        component: OfficeHome
      },
      {
        path: 'new',
        name: 'new-testimony',
        component: NewTestimony
      },
      {
        path: 'edit/:id/:tab',
        name: 'edit-testimony',
        component: EditTestimony
      },
      ]
    },
  ]
})

router.beforeEach((to, from, next) => {
  // make sure to transfer the accessToken from localStorage to the store
  // also set axios authentication token
  if (!store.state.accessToken && localStorage.getItem('accessToken')) {
    store.commit('updateAccessToken', localStorage.getItem('accessToken'))
    axios.defaults.headers.common['Authorization'] = 'Token ' + store.state.accessToken
  }

  // protect pages that require authentication
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state.accessToken) {
    //if (!localStorage.getItem('accessToken')) {
      next({
        name: 'login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }

  // if they user goes to /login and they are already logged in, send them to office
  if (to.fullPath.includes('/login')) {
    if (store.state.accessToken) {
      next('/office')
    }
  }
  next()
})

export default router
