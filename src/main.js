import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'

import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'
import '@vuikit/theme'
import axios from 'axios'
import i18n from './i18n'

Vue.config.productionTip = false

Vue.use(Vuikit)
Vue.use(VuikitIcons)

axios.defaults.baseURL = process.env.VUE_APP_ROOT_API

// TODO: set this dynamically
axios.defaults.headers.common['Accept-Language'] = store.state.selectedContentLanguage || process.env.VUE_APP_DEFAULT_LANGUAGE

new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
