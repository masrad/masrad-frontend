import {Model} from 'vue-mc'


/**
 * About model
 */
export default class About extends Model {

    // Default attributes that define the "empty" state.
    defaults() {
        return {
            id:   1,
            name: String,
            logo: String,
            description: String,
            long_description: String,
        }
    }

    // Route configuration
    routes() {
        return {
            fetch: '/about/{id}/',
            save: '/about/{id}/',
        }
    }
}
