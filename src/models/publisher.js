import {Model} from 'vue-mc'

/**
* Publisher model
*/
export default class Publisher extends Model {

  defaults() {
    return {
      id: null,
      name: null,
    }
  }

  mutations() {
    return {
      id: id => Number(id) || null,
      name: name => name ? String(name) : null,
    }
  }

  // Route configuration
  routes() {
    return {
        fetch: '/publishers/{id}/',
        save: '/publishers/{id}/',
    }
  }
}
