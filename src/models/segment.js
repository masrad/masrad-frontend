import {Model} from 'vue-mc'
// import { lt } from 'vue-mc/validation' //number,
/**
 * Segment model
 */
export default class Transcript extends Model {

  defaults() {
    return {
      testimony: null,
      id: null,
      start: null,
      title: null,
      summary: null,
      keywords: [],
      subject: [],
    }
  }

  mutations() {
    return {
      testimony: testimony => testimony ? Number(testimony) : null,
      id: id => id ? Number(id) : null,
      start: start => start ? String(start) : null,
    // use the string representation until we figure out the HH:MM:SS representation problem
    //   start: (start) => {
    //     if (typeof start === 'number') {
    //       return start
    //     } else if (typeof start === 'string') {
    //       // assume it's of the form HH:MM:SS
    //       let hhmmss = start.split(':') //.map(n => Number(n))
    //       let seconds = 0
    //       seconds = seconds + Number(hhmmss.pop())  // seconds
    //       seconds = seconds + Number(hhmmss.pop()) * 60 // minutes
    //       seconds = seconds + Number(hhmmss.pop()) * 3600 // hours
    //       return seconds
    //     } else {
    //       return null
    //     }
    // },
      title: title => title ? String(title) : null,
      summary: summary => summary ? String(summary) : null,
      subject: subject => subject ? subject : [],
      keywords: keywords => keywords ? keywords : [],
    }
  }

  shouldPatch() {
    return true
  }

  validation() {
    return {
      // TODO: start: number.and(lt(24*60*60)), // assume that the maximum length of a clip is 24 hours
      // start: number(), // assume that the maximum length of a clip is 24 hours
    }
  }

  // Route configuration
  routes() {
    return {
      fetch: '/segments/{id}/',
      save: '/segments/{id}/',
      create: '/segments/',
    }
  }
}
