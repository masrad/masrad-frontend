import {Model} from 'vue-mc'

/**
 * Series model
 * Use name Series to avoid clashing with name Collection
 */
export default class Series extends Model {

    // Default attributes that define the "empty" state.
    defaults() {
        return {
            id: Number,
            name: '',
            identifier: '',
        }
    }

    // Route configuration
    routes() {
        return {
            fetch: '/collections/{id}',
            save: '/collections',
        }
    }
}
