import {Model} from 'vue-mc'

/**
 * User model
 */
export default class User extends Model {

  // Default attributes that define the "empty" state.
  defaults() {
    return {
      id: 'current',
      username: '',
      first_name: '',
      last_name: '',
    }
  }

  mutations() {
    return {
      id: (id) => Number(id) || 'current',
      username: String,
      first_name: String,
      last_name: String
    }
  }

  // Route configuration
  routes() {
    return {
      fetch: '/users/{id}',
      save: '/user',
    }
  }
}
