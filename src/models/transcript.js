import {Model} from 'vue-mc'

/**
 * Transcript model
 */
export default class Transcript extends Model {

  defaults() {
    return {
      id: null,
      text: null,
      workflow: null,
      public: false,
    }
  }

  mutations() {
    return {
      id: id => id ? Number(id) : null,
      text: text => text ? String(text) : null,
      workflow: Object,//workflow => workflow ? Number(workflow) : null,
      public: Boolean,
    }
  }

  shouldPatch () {
    return true
  }
  
  // Route configuration
  routes() {
    return {
      fetch: '/transcripts/{id}/',
      save: '/transcripts/{id}/',
    }
  }
}
