import {Model} from 'vue-mc'

/**
 * User model
 */
export default class Person extends Model {

  // Default attributes that define the "empty" state.
  defaults() {
    return {
      id: null,
      name: null,
      identifier: null,
      date_of_birth: null,
      address: null,
      phone: null,
      alternative_communication: null
    }
  }

  mutations() {
    return {
      id: id => Number(id) || null,
      name: name => name ? String(name) : null,
      identifier: identifier => identifier ? String(identifier) : null,
      date_of_birth: date_of_birth => date_of_birth ? new Date(date_of_birth) : null,
      address: address => address ? String(address) : null,
      phone: phone => phone ? String(phone) : null,
      alternative_communication: alternative_communication => alternative_communication ? String(alternative_communication) : null
    }
  }

  shouldPatch () {
    return true
  }

  // Route configuration
  routes() {
    return {
      fetch: '/persons/{id}',
      save: '/persons/{id}',
    }
  }
}
