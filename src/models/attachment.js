import {Model} from 'vue-mc'

/**
 * Attachment model
 */
export default class Attachment extends Model {

  defaults() {
    return {
      id: null,
      name: null,
      description: null,
      file: null,
    }
  }

  mutations() {
    return {
      id: id => id ? Number(id) : null,
      name: name => name ? String(name) : null,
      description: description => description ? String(description) : null,
      file: file => file ? String(file) : null,
    }
  }

  shouldPatch () {
    return true
  }
  
  // Route configuration
  routes() {
    return {
      fetch: '/attachments/{id}/',
      save: '/attachments/{id}/',
      delete: '/attachments/{id}/',
    }
  }
}
