import {Model} from 'vue-mc'

/**
 * Keyword model
 */
export default class Keyword extends Model {

  defaults() {
    return {
      id: null,
      name: null,
    }
  }

  mutations() {
    return {
      id: id => Number(id) || null,
      name: name => name ? String(name) : null,
    }
  }

  // Route configuration
  routes() {
    return {
      fetch: '/keywords/{id}/',
      save: '/keywords/{id}/',
    }
  }
}
