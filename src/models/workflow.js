import {Model} from 'vue-mc'

/**
* Workflow model
*/
export default class Workflow extends Model {

  // Default attributes that define the "empty" state.
  defaults() {
    return {
      id: null,
      content_complete: '',
      anonymization_complete: '',
      quality_control_complete: '',
    }
  }

  mutations() {
    return {
      id: id => Number(id),
      content_complete: Boolean,
      anonymization_complete: Boolean,
      quality_control_complete: Boolean,
    }
  }

  shouldPatch () {
    return true
  }

  // Route configuration
  routes() {
    return {
        fetch: '/workflows/{id}/',
        save: '/workflows/{id}/',
    }
  }
}
