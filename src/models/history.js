import {Model} from 'vue-mc'

/**
 * History model
 */
export default class User extends Model {

  // Default attributes that define the "empty" state.
  defaults() {
    return {
      history_id: null,
      change_user: null,
      change_fields: null,
      history_user: null,
      history_date: null,
    }
  }

  options() {
    return {
      identifier: 'history_id',
    }
  }

  mutations() {
    return {
      id: (id) => Number(id),
      change_user: String,
      change_fields: Array,
      history_user: Number,
      history_date: history_date => history_date ? new Date(history_date) : null,
    }
  }

  // Route configuration
  routes() {
    return {
      fetch: '/history/{id}',
    }
  }
}
