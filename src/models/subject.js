import {Model} from 'vue-mc'

/**
 * Subject model
 */
export default class Subject extends Model {

  defaults() {
    return {
      id: null,
      name: null,
    }
  }

  mutations() {
    return {
      id: id => Number(id) || null,
      name: name => name ? String(name) : null,
    }
  }

  // Route configuration
  routes() {
    return {
      fetch: '/subjects/{id}',
      save: '/subjects/{id}',
    }
  }
}
