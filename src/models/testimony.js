import {Model} from 'vue-mc'

/**
* Testimony model
*/
export default class Testimony extends Model {

  // Default attributes that define the "empty" state.
  defaults() {
    return {
      id: null,
      title: '',
      dc_identifier: null,
      description: '',
      relation: null,
      publisher: null,
      date: null,
      location: null,
      file: '',
      coverage: null,
      creator: null,
      contributor: null,
      subject: [],
      keywords: [],
      language: null,
      rights: null,
      file_size: '',
      files_public: false,
      metadata_public: false,
      published: false,
      release_form: null,
      release_date: null,
      modified: null,
    }
  }

  mutations() {
    return {
      id: id => Number(id),
      title: String,
      description: description => description ? String(description) : null,
      dc_identifier: dc_identifier => dc_identifier ? String(dc_identifier) : null,
      relation: relation => relation ? Number(relation) : null,
      publisher: publisher => publisher ? Number(publisher) : null,
      date: date => date ? new Date(date) : null,
      coverage: coverage => coverage ? String(coverage) : ' ; -',
      file: file => file ? String(file) : null,
      location: location => location ? String(location) : null,
      creator: creator => Number(creator) || null,
      contributor: contributor => Number(contributor) || null,
      subject: subject => subject ? subject : [],
      keywords: keywords => keywords ? keywords : [],
      anonymized_file: anonymized_file => anonymized_file ? String(anonymized_file) : null,
      language: language => language ? String(language) : null,
      file_size: file_size => file_size ? String(file_size) : null,
      rights: rights => rights ? String(rights) : null,
      files_public: Boolean,
      metadata_public: Boolean,
      published: Boolean,
      release_form: release_form => release_form ? String(release_form) : null,
      release_date: release_date => release_date ? String(release_date) : null,
      modified:  modified => modified ? new Date(modified) : null,
    }
  }

  shouldPatch () {
    return true
  }

  // Route configuration
  routes() {
    return {
        fetch: '/testimonies/{id}/',
        save: '/testimonies/{id}/',
    }
  }
}
