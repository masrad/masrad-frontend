# masrad-frontend

The frontend component for the MASRAD:platform. See [masrad-deployment](https://gitlab.com/masrad/masrad-deployment/) for deployment options. 

To try this out locally, make sure that you create a `.env.local` file with your local settings, especially setting `VUE_APP_ROOT_API='http://127.0.0.1:8000/api'`. 


# Default Vue.js instructions

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
